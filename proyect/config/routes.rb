Rails.application.routes.draw do
  get 'recipe/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'recipe#index'
end
